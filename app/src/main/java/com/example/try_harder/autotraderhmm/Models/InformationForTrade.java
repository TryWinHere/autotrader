package com.example.try_harder.autotraderhmm.Models;

/**
 * Created by Try_harder on 30.07.2015.
 */
public class InformationForTrade {

    String currentElementNameRus;
    String currentElementName;
    String currentTradeNumber;
    boolean tradeWork = true;
    long finishDate;
    long timeLeft;
    int delayForTrade;
    int delayForBuy;
    int delayForFastBuy;
    int dispersionDelayForFastBuy;
    int dispersionDelayForBuy;
    int neededCountForBuy;
    int topPrice;
    int priceForFastBuy;
    int boughtCount;
    int countOfSuccessBuy;
    int totalCountOfBuy;
    int countOfSuccessFastBuy;
    int totalCountOfFastBuy;

    public int getCountOfSuccessFastBuy() {
        return countOfSuccessFastBuy;
    }

    public void setCountOfSuccessFastBuy(int countOfSuccessFastBuy) {
        this.countOfSuccessFastBuy = countOfSuccessFastBuy;
    }

    public int getTotalCountOfFastBuy() {
        return totalCountOfFastBuy;
    }

    public void setTotalCountOfFastBuy(int totalCountOfFastBuy) {
        this.totalCountOfFastBuy = totalCountOfFastBuy;
    }

    public InformationForTrade() {
    }

    public String getCurrentElementName() {
        return currentElementName;
    }

    public void setCurrentElementName(String currentElementName) {
        this.currentElementName = currentElementName;
    }

    public int getNeededCountForBuy() {
        return neededCountForBuy;
    }

    public void setNeededCountForBuy(int neededCountForBuy) {
        this.neededCountForBuy = neededCountForBuy;
    }

    public long getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(long finishDate) {
        this.finishDate = finishDate;
    }

    public long getDelayForTrade() {
        return delayForTrade;
    }

    public void setDelayForTrade(int delayForTrade) {
        this.delayForTrade = delayForTrade;
    }

    public int getTopPrice() {
        return topPrice;
    }

    public void setTopPrice(int topPrice) {
        this.topPrice = topPrice;
    }


    public int getBoughtCount() {
        return boughtCount;
    }

    public void setBoughtCount(int boughtCount) {
        this.boughtCount = boughtCount;
    }

    public boolean isTradeWork() {
        return tradeWork;
    }

    public void setTradeWork(boolean tradeWork) {
        this.tradeWork = tradeWork;
    }

    public String getCurrentElementNameRus() {
        return currentElementNameRus;
    }

    public void setCurrentElementNameRus(String currentElementNameRus) {
        this.currentElementNameRus = currentElementNameRus;
    }


    public String getCurrentTradeNumber() {
        return currentTradeNumber;
    }

    public void setCurrentTradeNumber(String currentTradeNumber) {
        this.currentTradeNumber = currentTradeNumber;
    }

    public int getCountOfSuccessBuy() {
        return countOfSuccessBuy;
    }

    public void setCountOfSuccessBuy(int countOfSuccessBuy) {
        this.countOfSuccessBuy = countOfSuccessBuy;
    }

    public int getTotalCountOfBuy() {
        return totalCountOfBuy;
    }

    public void setTotalCountOfBuy(int totalCountOfBuy) {
        this.totalCountOfBuy = totalCountOfBuy;
    }

    public long getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(long timeLeft) {
        this.timeLeft = timeLeft;
    }

    public long getDelayForBuy() {
        return delayForBuy;
    }

    public void setDelayForBuy(int delayForBuy) {
        this.delayForBuy = delayForBuy;
    }

    public int getDispersionDelayForBuy() {
        return dispersionDelayForBuy;
    }

    public void setDispersionDelayForBuy(int dispersionDelayForBuy) {
        this.dispersionDelayForBuy = dispersionDelayForBuy;
    }

    public int getPriceForFastBuy() {
        return priceForFastBuy;
    }

    public void setPriceForFastBuy(int priceForFastBuy) {
        this.priceForFastBuy = priceForFastBuy;
    }


    public long getDelayForFastBuy() {
        return delayForFastBuy;
    }

    public void setDelayForFastBuy(int delayForFastBuy) {
        this.delayForFastBuy = delayForFastBuy;
    }

    public int getDispersionDelayForFastBuy() {
        return dispersionDelayForFastBuy;
    }

    public void setDispersionDelayForFastBuy(int dispersionDelayForFastBuy) {
        this.dispersionDelayForFastBuy = dispersionDelayForFastBuy;
    }

}
