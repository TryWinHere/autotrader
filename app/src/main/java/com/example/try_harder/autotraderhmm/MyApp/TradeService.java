package com.example.try_harder.autotraderhmm.myApp;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import static com.example.try_harder.autotraderhmm.myApp.Constants.*;

import com.example.try_harder.autotraderhmm.Models.InformationForTrade;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;


/**
 * Created by Try_harder on 23.07.2015.
 */
public class TradeService extends Service{
    @Nullable

    final String LOG_TAG = "myLogs";
    private ThreadPoolExecutor mPool;
    Firebase myBase;
    private HashMap<String, Trader> traderMap = new HashMap<>();

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind");
        return null;
    }

    public void onCreate() {
        super.onCreate();
        mPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(CORE_POOL_SIZE);
        myBase = new Firebase(MY_BASE_URL);
        myBase.addChildEventListener(createEventListener());
                Log.d(LOG_TAG, "onCreate");
    }

    private ChildEventListener createEventListener(){
        return new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    mPool.submit(startTrader(dataSnapshot));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                traderMap.get(dataSnapshot.getKey()).setInformationForTrade(dataSnapshot.getValue(InformationForTrade.class));
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String currentTraderNumber = dataSnapshot.getValue(InformationForTrade.class).getCurrentTradeNumber();
                traderMap.get(currentTraderNumber).removeTrader();
                traderMap.remove(currentTraderNumber);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        };
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    private Runnable startTrader(final DataSnapshot dataSnapshot){
        return (new Runnable() {
            @Override
            public void run() {
                traderMap.put(dataSnapshot.getKey(), new Trader(dataSnapshot.getValue(InformationForTrade.class)));
            }
        });
    }

    public void onDestroy() {
        super.onDestroy();
        for (HashMap.Entry<String,Trader> trader : traderMap.entrySet()) {
            myBase.child(trader.getKey()).child(TRADE_WORK).setValue(false);
        }
        Log.d(LOG_TAG, "onDestroy");
    }


}
