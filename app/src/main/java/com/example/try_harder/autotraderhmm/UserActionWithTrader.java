package com.example.try_harder.autotraderhmm;

import com.example.try_harder.autotraderhmm.Models.InformationForTrade;

/**
 * Created by Try_harder on 03.08.2015.
 */
public interface UserActionWithTrader {
    void updateTrader(String traderNumber);
}
