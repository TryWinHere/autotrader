package com.example.try_harder.autotraderhmm.Models;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.client.Response;

/**
 * Created by Try_harder on 29.07.2015.
 */
public abstract class PageParser {

    public abstract Elements getMainElement(Document document);

    public Document getDocumentFromStringResponse(String convertedResponse) {
        return Jsoup.parse(convertedResponse);
    }

    public String responseToStringConvert(Response response){
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {

            reader = new BufferedReader(new InputStreamReader(response.getBody().in(), "windows-1251"));

            String line;

            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
}
