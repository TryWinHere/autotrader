package com.example.try_harder.autotraderhmm.myApp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.try_harder.autotraderhmm.Models.InformationForTrade;
import com.example.try_harder.autotraderhmm.R;
import com.example.try_harder.autotraderhmm.RecyclerViewAdapter;
import com.example.try_harder.autotraderhmm.UserActionWithTrader;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import static com.example.try_harder.autotraderhmm.myApp.Constants.*;

import java.util.ArrayList;

/**
 * Created by Try_harder on 31.07.2015.
 */
public class RecyclerViewForTraderActivity extends Activity implements UserActionWithTrader{

    private ArrayList<InformationForTrade> informationOfActiveTradeList = new ArrayList<>();
    private ArrayList<InformationForTrade> informationOfInactiveTradeList = new ArrayList<>();
    int arrayListSize;
    RecyclerViewAdapter adapter;
    Firebase myBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myBase = new Firebase(MY_BASE_URL);
        setContentView(R.layout.activity_recycleviewfortrader);

        myBase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                InformationForTrade informationForTrade = dataSnapshot.getValue(InformationForTrade.class);
                if (informationForTrade.isTradeWork()) {
                    informationOfActiveTradeList.add(informationForTrade);
                } else {
                    informationOfInactiveTradeList.add(informationForTrade);
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue(InformationForTrade.class).isTradeWork()) {
                updateInformationForTradeList(dataSnapshot.getValue(InformationForTrade.class));
                adapter.notifyDataSetChanged(); }
                else {
                    removeElementFromInformationTradeList(dataSnapshot.getValue(InformationForTrade.class).getCurrentTradeNumber());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                //     removeElementFromInformationTradeList(dataSnapshot.getValue(InformationForTrade.class).getCurrentTradeNumber());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        if (getIntent().getAction().equals(SHOW_ACTIVE_TRADERS)) {
            adapter = new RecyclerViewAdapter(informationOfActiveTradeList, this, ACTIVE_LIST);
        } if (getIntent().getAction().equals(SHOW_INACTIVE_TRADERS)) {
            adapter = new RecyclerViewAdapter(informationOfInactiveTradeList, this, INACTIVE_LIST);
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(itemAnimator);

    }

    private void removeElementFromInformationTradeList(String traderNumber){

        int numberInArrayList = 0;
        while (informationOfActiveTradeList.size() > numberInArrayList){
            if (informationOfActiveTradeList.get(numberInArrayList).getCurrentTradeNumber().equals(traderNumber)){
                informationOfActiveTradeList.remove(numberInArrayList);
            }
            numberInArrayList++;
        }
    }

    private void updateInformationForTradeList(InformationForTrade informationForTrade) {

        int numberInArrayList = 0;
        while (informationOfActiveTradeList.size() > numberInArrayList){
            if (informationOfActiveTradeList.get(numberInArrayList).getCurrentTradeNumber().equals(informationForTrade.getCurrentTradeNumber())){
                informationOfActiveTradeList.set(numberInArrayList, informationForTrade);
            }
            numberInArrayList++;
        }
    }

    //TODO: Реализации update очень похожи на костыль, ничего пока придумать не могу.

    private boolean isListFinish(int currentNumber) {
        return currentNumber < arrayListSize;
    }


    @Override
    public void updateTrader(String traderNumber) {
        myBase.child(traderNumber).removeValue();
    }
}



