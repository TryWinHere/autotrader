package com.example.try_harder.autotraderhmm;

import com.example.try_harder.autotraderhmm.Models.PageParser;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;

import retrofit.client.Response;

import static com.example.try_harder.autotraderhmm.myApp.Constants.*;

/**
 * Created by Try_harder on 22.07.2015.
 */
public class ParsePageForLotInformation extends PageParser {

    HashMap<String,String> lotInformation;

    public HashMap<String, String> parsePage(Response response) {
        parseDocForLotInformation(getDocumentFromStringResponse(responseToStringConvert(response)));
        return lotInformation;
    }

    private void parseDocForLotInformation(Document document){
            getInformation(getMainElement(document).get(FIRST_LOT_NUMBER));
    }

    protected void getInformation(final Element mainElement) {
        lotInformation = new HashMap<String, String>(){
                {
                    put(SHOW_PRICE, parseElementForPrice(mainElement));
                    put(TRADER_GOT_COUNT, parseElementForCount(mainElement));
                    put(LOT_ID, parseElementForLotId(mainElement));
                }};
    }

    private String parseElementForLotId(Element element){
        return cleanLotIdString(getLotIdFromElement(element));
            }

    private String parseElementForPrice(Element element){
        return cleanPriceString(getPriceFromElement(element));
    }

    private String parseElementForCount(Element element){
        return cleanCountString(getCountFromElement(element));
    }

    private String cleanLotIdString(String lot_id){
        return lot_id.substring(lot_id.indexOf("\"") + 1, lot_id.lastIndexOf("\""));
    }

    private String cleanPriceString(String price){
        return price.replace(",", "");
    }

    private String cleanCountString(String count){
        return count.substring(count.indexOf(">") + 1, count.indexOf(" "));
    }

    private String getLotIdFromElement(Element element) {
        return element.select("a").get(0).toString();
    }

    private String getPriceFromElement(Element element) {
        String price = element.select("[border=0]").select("tr").select("td").get(1).toString();
        price = price.substring(price.indexOf(">") + 1,price.lastIndexOf("<"));
        return price;
    }

    private String getCountFromElement(Element element) {
        return element.select("[valign=top]").get(0).select("b").get(1).toString();
    }

    @Override
    public Elements getMainElement(Document document) {
        return document.select(".wb");
    }
}
