package com.example.try_harder.autotraderhmm.myApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.try_harder.autotraderhmm.Models.InformationForTrade;
import com.example.try_harder.autotraderhmm.R;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import static com.example.try_harder.autotraderhmm.myApp.Constants.*;

/**
 * Created by Try_harder on 08.08.2015.
 */
public class PreferencesActivity extends Activity {

    public static final String MESSAGE = "Введите значение(мс):";
    public static final String TITLE = "Ввод данных";
    public static final String MESSAGE_DEFAULT_SET = "Вы точно хотите сбросить настройки?";
    public static final String TITLE_DEFAULT = "Сбросить настройки";
    TextView tvDelayForBuy;
    TextView tvDelayForFastBuy;
    TextView tvDispersionDelayForBuy;
    TextView tvDispersionDelayForFastBuy;
    TextView tvDelayForTrade;
    TableRow trDelayBuy;
    TableRow trDelayFastBuy;
    TableRow trDispersionBuy;
    TableRow trDispersionFastBuy;
    TableRow trDelayTrade;
    SharedPreferences sPref;

    Firebase myBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);
        sPref = this.getSharedPreferences(PREF_PATH, MODE_PRIVATE);

        myBase = new Firebase(MY_BASE_URL);

        tvDelayForBuy = (TextView) findViewById(R.id.tvDelayForBuy);
        tvDelayForFastBuy = (TextView) findViewById(R.id.tvDelayForFastBuy);
        tvDelayForTrade = (TextView) findViewById(R.id.tvDelayForTrade);
        tvDispersionDelayForBuy = (TextView) findViewById(R.id.tvDispersionDelayForBuy);
        tvDispersionDelayForFastBuy = (TextView) findViewById(R.id.tvDispersionDelayForFastBuy);

        tvDelayForBuy.setText(TEXT_DELAY_FOR_BUY + getIntPreferences(DELAY_FOR_BUY, DEFAULT_DELAY_FOR_BUY));
        tvDelayForFastBuy.setText(TEXT_DELAY_FOR_FAST_BUY + getIntPreferences(DELAY_FOR_FAST_BUY, DEFAULT_DELAY_FOR_FAST_BUY));
        tvDelayForTrade.setText(TEXT_DELAY_FOR_TRADE + getIntPreferences(DELAY_FOR_TRADE, DEFAULT_DELAY_FOR_TRADE));
        tvDispersionDelayForBuy.setText(TEXT_DISPERSION_FOR_BUY + getIntPreferences(DISPERSION_FOR_BUY, DEFAULT_DISPERSION_DELAY_FOR_BUY));
        tvDispersionDelayForFastBuy.setText(TEXT_DISPERSION_FOR_FAST_BUY + getIntPreferences(DISPERSION_FOR_FAST_BUY, DEFAULT_DISPERSION_DELAY_FOR_FAST_BUY));

        trDelayBuy = (TableRow) findViewById(R.id.rowDelayForBuy);
        trDelayFastBuy = (TableRow) findViewById(R.id.rowDelayForFastBuy);
        trDispersionBuy = (TableRow) findViewById(R.id.rowDispersionForBuy);
        trDispersionFastBuy = (TableRow) findViewById(R.id.rowDelayForFastBuy);
        trDelayTrade = (TableRow) findViewById(R.id.rowTradeDelay);

        trDelayBuy.setOnClickListener(onClickListener(DELAY_FOR_BUY, this));
        trDelayFastBuy.setOnClickListener(onClickListener((DELAY_FOR_FAST_BUY), this));
        trDispersionBuy.setOnClickListener(onClickListener(DISPERSION_FOR_BUY, this));
        trDispersionFastBuy.setOnClickListener(onClickListener(DELAY_FOR_FAST_BUY, this));
        trDelayTrade.setOnClickListener(onClickListener(DELAY_FOR_TRADE, this));

    }

    private View.OnClickListener onClickListener(final String key, final Context context) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dateEnterDialog = new AlertDialog.Builder(context);
                final EditText etData= new EditText(context);
                etData.setInputType(InputType.TYPE_CLASS_NUMBER);

                dateEnterDialog.setMessage(MESSAGE);
                dateEnterDialog.setTitle(TITLE);

                dateEnterDialog.setView(etData);

                dateEnterDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //What ever you want to do with the value
                        String value = etData.getText().toString();
                        updateTextView(key, value);
                        final int userData = Integer.parseInt(value);
                        SharedPreferences.Editor editor = sPref.edit();
                        editor.putInt(key, userData);
                        editor.commit();
                        myBase.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    for (DataSnapshot loopBase : dataSnapshot.getChildren()) {
                                        if (loopBase.getValue(InformationForTrade.class).isTradeWork()) {
                                            myBase.child(loopBase.getKey()).child(key).setValue(userData);
                                        }
                                    }
                                    createToastForDataChanged().show();

                                }
                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {

                            }
                        });


                    }
                });

                dateEnterDialog.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });

                dateEnterDialog.show();
            }
        };
    }

    private void updateTextView(String key, String value){
        switch (key){
            case(DELAY_FOR_BUY):
                tvDelayForBuy.setText(TEXT_DELAY_FOR_BUY + value);
                break;
            case(DELAY_FOR_FAST_BUY):
                tvDelayForFastBuy.setText(TEXT_DELAY_FOR_FAST_BUY + value);
                break;
            case(DELAY_FOR_TRADE):
                tvDelayForTrade.setText(TEXT_DELAY_FOR_TRADE + value);
                break;
            case(DISPERSION_FOR_BUY):
                tvDispersionDelayForBuy.setText(DISPERSION_FOR_BUY + value);
                break;
            case(DISPERSION_FOR_FAST_BUY):
                tvDispersionDelayForFastBuy.setText(DISPERSION_FOR_FAST_BUY + value);
        }
    }

    protected Toast createToastForDataChanged() {
        return Toast.makeText(getApplicationContext(),
                R.string.toast_data_changed,
                Toast.LENGTH_SHORT);
    }

    private String getIntPreferences(String key, int defaultValue){
        return String.valueOf(sPref.getInt(key, defaultValue));
    }

    public void setDefaultPreferences(){
        AlertDialog.Builder dateEnterDialog = new AlertDialog.Builder(this);
        dateEnterDialog.setMessage(MESSAGE_DEFAULT_SET);
        dateEnterDialog.setTitle(TITLE_DEFAULT);
    }

}
