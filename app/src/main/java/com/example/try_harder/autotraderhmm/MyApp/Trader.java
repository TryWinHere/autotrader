package com.example.try_harder.autotraderhmm.myApp;

import com.example.try_harder.autotraderhmm.Models.InformationForTrade;
import com.example.try_harder.autotraderhmm.LotInformationResponse;
import com.example.try_harder.autotraderhmm.Network;
import com.example.try_harder.autotraderhmm.TradeInterface;
import com.firebase.client.Firebase;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.example.try_harder.autotraderhmm.myApp.Constants.LONG_DELAY;
import static com.example.try_harder.autotraderhmm.myApp.Constants.LOT_ID;
import static com.example.try_harder.autotraderhmm.myApp.Constants.MY_BASE_URL;
import static com.example.try_harder.autotraderhmm.myApp.Constants.NORMAL_DELAY;
import static com.example.try_harder.autotraderhmm.myApp.Constants.SHOW_PRICE;

/**
 * Created by Try_harder on 30.07.2015.
 */
public class Trader implements LotInformationResponse, TradeInterface {

    private InformationForTrade informationForTrade;
    String buyCount;
    String currentLotPrice;
    final String FAST_BUY_MANY = "11";
    final String FAST_BUY_ONE = "1";
    final int SPECIAL_VALUE = 9;
    boolean doFastBuy;
    int boughtCount;
    int leftCount;
    Firebase firebase;

    private final ScheduledExecutorService delayedTrader =
            Executors.newSingleThreadScheduledExecutor();

    private final ScheduledExecutorService everySecondUpdaterForTradeInformation = Executors.newSingleThreadScheduledExecutor();

    private final ScheduledExecutorService delayedBuy = Executors.newSingleThreadScheduledExecutor();


    public Trader(InformationForTrade informationForTrade) {
        this.informationForTrade = informationForTrade;
        leftCount = informationForTrade.getNeededCountForBuy() - informationForTrade.getBoughtCount();
        firebase = new Firebase(MY_BASE_URL);
        if (informationForTrade.isTradeWork()) {
            trade();
            throwUpdater();
        }
    }

    void trade() {
        Network.getInstance().lotInformation(informationForTrade.getCurrentElementName(), this);
    }

    @Override
    public void catchLotInformation(HashMap<String,String> information) {
        currentLotPrice = information.get(SHOW_PRICE);
        if (priceIsAcceptable())
        {
            doFastBuy = isPriceForFastBuy();
            updateBuyCount();
            if (doFastBuy) {
                doDelayedBuy(information.get(LOT_ID), informationForTrade.getDelayForFastBuy(), informationForTrade.getDispersionDelayForBuy());
            } else {
                doDelayedBuy(information.get(LOT_ID), informationForTrade.getDelayForBuy(), informationForTrade.getDispersionDelayForBuy());
            }

        }
        else if (checkConditionForNextTrade()){
            delayedNextTrade(NORMAL_DELAY);
        } else { } //TODO: TraderOFF realization

    }

    public void setInformationForTrade(InformationForTrade informationForTrade) {
        this.informationForTrade = informationForTrade;
    }

    private void updateBuyCount() {
        if (doFastBuy){
            buyCount = FAST_BUY_ONE;
        } else if (isLeftCountCloseToFinish()) {
            buyCount = FAST_BUY_MANY;
        } else {
                buyCount = String.valueOf(leftCount);
            }
        }


    private boolean isPriceForFastBuy(){
        return (Integer.parseInt(currentLotPrice) <= informationForTrade.getPriceForFastBuy());
    }

    private boolean isLeftCountCloseToFinish() {
        return leftCount > SPECIAL_VALUE;
    }

    private void buyElement(String lot_id) {
        Network.getInstance().buyElement(lot_id,buyCount,this);
    }

    private boolean priceIsAcceptable() {
        return (Integer.parseInt(currentLotPrice) <= informationForTrade.getTopPrice());
    }

    @Override
    public void tradeCatch(int boughtCount) {
        if (isTradeSuccess(boughtCount)) {
            successTradeUpdater(boughtCount);
        }
        if (doFastBuy){
            updateCountOfTotalFastBuy();
        } else {
            updateTotalCountOfBuy();
        }
        doNextTradeIfNeed();
    }

    private void successTradeUpdater(int boughtCount) {
        updateBoughtCount(boughtCount);
        updateLeftCount(boughtCount);
        if (doFastBuy){
            updateCountOfSuccessFastBuy();
        } else {
        updateCountOfSuccessBuy(); }
    }

    private void updateCountOfSuccessFastBuy() {
        informationForTrade.setCountOfSuccessFastBuy(informationForTrade.getCountOfSuccessFastBuy() + 1);
    }

    private void updateCountOfTotalFastBuy() {
        informationForTrade.setTotalCountOfFastBuy(informationForTrade.getTotalCountOfFastBuy() + 1);
    }

    private void updateTotalCountOfBuy() {
        informationForTrade.setTotalCountOfBuy(informationForTrade.getTotalCountOfBuy() + 1);
    }

    private void updateCountOfSuccessBuy() {
        informationForTrade.setCountOfSuccessBuy(informationForTrade.getCountOfSuccessBuy() + 1);
    }

    private boolean isTradeSuccess(int boughtCount) {
        return boughtCount > 0;
    }

    private void updateBoughtCount(int boughtCount){
        this.boughtCount = this.boughtCount + boughtCount;
        informationForTrade.setBoughtCount(this.boughtCount);
    }

    private void updateLeftCount(int boughtCount){
        leftCount = leftCount - boughtCount;
    }

    private void doNextTradeIfNeed() {
        if (checkConditionForNextTrade())
            delayedNextTrade(LONG_DELAY);
        else stopTrader();
    }

    protected boolean checkConditionForNextTrade() {
        return (isBuyEnough() & (isDateCome()) & informationForTrade.isTradeWork());
    }

    private boolean isBuyEnough() {
        return (informationForTrade.getNeededCountForBuy() > informationForTrade.getBoughtCount());
    }

    private boolean isDateCome(){
        return (informationForTrade.getTimeLeft() > 0);
    }

    void delayedNextTrade(int multiple) {

        Runnable task = new Runnable() {
            public void run() {

                trade();
            }
        };
        delayedTrader.schedule(task, madeDelay(multiple), TimeUnit.MILLISECONDS);
    }

    void doDelayedBuy(final String lot_id, long currentDelay, int currentDispersion){

        Runnable task = new Runnable() {
            @Override
            public void run() {
                buyElement(lot_id);
            }
        };
        delayedBuy.schedule(task, madeDelayForBuy(currentDelay, currentDispersion), TimeUnit.MILLISECONDS);

    }

    private long madeDelayForBuy(long currentDelay, int currentDispersion){
        return currentDelay + (new Random().nextInt(currentDispersion) - currentDispersion/2);
    }

    private long madeDelay(int multiple) {
        return informationForTrade.getDelayForTrade()*multiple*(new Random().nextInt(4) + 1);
    }

    public void removeTrader(){
        informationForTrade.setTradeWork(false);
    }

    public void stopTrader() {
        informationForTrade.setTradeWork(false);
        firebase.child(informationForTrade.getCurrentTradeNumber()).child("tradeWork").setValue(false);
    }

    public void resumeTrader(){
        informationForTrade.setFinishDate(informationForTrade.getTimeLeft() + System.currentTimeMillis());
        trade();
    }

    void throwUpdater() {
        Runnable task = new Runnable() {
            @Override
            public void run() {

                if (informationForTrade.isTradeWork()) {
                    informationForTrade.setTimeLeft(informationForTrade.getFinishDate() - System.currentTimeMillis());
                    firebase.child(informationForTrade.getCurrentTradeNumber()).setValue(informationForTrade);
                    if (isDateCome()) {
                        throwUpdater();
                    } else {
                        informationForTrade.setTradeWork(false);
                        firebase.child(informationForTrade.getCurrentTradeNumber()).child("tradeWork").setValue(false);
                    }
                }

                }
        };
         everySecondUpdaterForTradeInformation.schedule(task, 1000, TimeUnit.MILLISECONDS);
    }
}
