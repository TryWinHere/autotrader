package com.example.try_harder.autotraderhmm.myApp;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.try_harder.autotraderhmm.myApp.Constants.*;

import com.example.try_harder.autotraderhmm.LotInformationResponse;
import com.example.try_harder.autotraderhmm.Models.InformationForTrade;
import com.example.try_harder.autotraderhmm.Network;
import com.example.try_harder.autotraderhmm.R;
import com.example.try_harder.autotraderhmm.CatchLoginInfo;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements LotInformationResponse, CatchLoginInfo {

    SharedPreferences  sPref;
    String currentTradeNumberString = "0";
    Firebase myAppBase;
    int currentTradeNumber = 0;
    int elementTopPrice;
    int elementCountForBuy;
    int priceForFastBuy;
    InformationForTrade informationAboutCurrentTrade = new InformationForTrade();
    Intent intent;

    public TextView tvCurrentPriceHint;
    public EditText etTopPriceLine;
    public EditText etElementsCountForBuy;
    public EditText etTraderTimeLive;
    public EditText etPriceForFastBuy;
    private static HashMap<String, String> elementsNameMap = createHashMapForElements();
    private String currentElementName;
    private String currentElementRusName;
    private long traderFinishDateInMs;
    private String[] massiveOfElements;
    long traderTimeLiveInMs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sPref = getSharedPreferences(PREF_PATH, this.MODE_PRIVATE);

        createStringMassiveOfElements();
        Firebase.setAndroidContext(this);
        myAppBase = new Firebase(MY_BASE_URL);

        myAppBase.addListenerForSingleValueEvent(valueEventListener());

        etElementsCountForBuy = (EditText) findViewById(R.id.etElementsCountForBuy);
        etTopPriceLine = (EditText) findViewById(R.id.etTopPriceLine);
        etTraderTimeLive = (EditText) findViewById(R.id.etTimeLiveForTrade);
        etPriceForFastBuy = (EditText) findViewById(R.id.etPriceForFastBuy);

        if (!isMyServiceRunning(TradeService.class)) {
            Network.getInstance().loginHmmRetrofit();
            createIntentForTradeService();
            startTradeService();
        }//логинимся



        tvCurrentPriceHint = (TextView) findViewById(R.id.tvCurrentLowPrice);

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);

        spinner.setAdapter(makeAdapter()); // Вызываем и назначаем адаптер
        spinner.setOnItemSelectedListener(getOnSpinnerClickListener());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(intent);
    }

    public void showActiveTraders(View view) {
        startActivity(createIntentActiveTradersForRecyclerViewActivity());
    }

    public void showInactiveTraders(View view){
        startActivity(createIntentInactiveTradersForRecyclerViewActivity());

    }

    private Intent createIntentInactiveTradersForRecyclerViewActivity(){
        Intent intent = new Intent(this, RecyclerViewForTraderActivity.class);
        intent.setAction(SHOW_INACTIVE_TRADERS);
        return intent;
    }

    private Intent createIntentActiveTradersForRecyclerViewActivity() {
        Intent intent = new Intent(this, RecyclerViewForTraderActivity.class);
        intent.setAction(SHOW_ACTIVE_TRADERS);
        return intent;
    }

    //обработка нажатия на элемент

    AdapterView.OnItemSelectedListener getOnSpinnerClickListener() {
        return new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {

                getLotInformation(selectedItemPosition);


            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
    }


    //**************************************

    private ValueEventListener valueEventListener() {
        return (new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot loopBase : dataSnapshot.getChildren()) {
                        currentTradeNumber = Integer.parseInt(loopBase.getKey()) + 1; //TODO: Переработать. Поиск индекса для следующего трейда.
                    }

                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    // Стартуем трейдер по нажатию

    public void onClickStartTrader(View v) {

        preparationForTrade();
        if (checkConditionsForStartTrade()){
            successfulTradeStartMethods();
        }
        else {
            createToastForMissingValue().show();
        }
    }

    //*********************************

    private void preparationForTrade() {
        updateInformationAboutTraderFromET();
        updateStringNumber();
        feelInformationForTrade();
    }

    private void updateStringNumber() {
        currentTradeNumberString = String.valueOf(currentTradeNumber);
    }

    private void feelInformationForTrade(){
        informationAboutCurrentTrade = new InformationForTrade();
        informationAboutCurrentTrade.setNeededCountForBuy(elementCountForBuy);
        informationAboutCurrentTrade.setFinishDate(traderFinishDateInMs);
        informationAboutCurrentTrade.setTopPrice(elementTopPrice);
        informationAboutCurrentTrade.setCurrentElementName(currentElementName);
        informationAboutCurrentTrade.setCurrentElementNameRus(currentElementRusName);
        informationAboutCurrentTrade.setCurrentTradeNumber(currentTradeNumberString);
        informationAboutCurrentTrade.setTimeLeft(traderTimeLiveInMs);
        informationAboutCurrentTrade.setDelayForTrade(getValueFromPreferences(DELAY_FOR_TRADE, DEFAULT_DELAY_FOR_TRADE));
        informationAboutCurrentTrade.setDelayForBuy(getValueFromPreferences(DELAY_FOR_BUY, DEFAULT_DELAY_FOR_BUY));
        informationAboutCurrentTrade.setDelayForFastBuy(getValueFromPreferences(DELAY_FOR_FAST_BUY, DEFAULT_DISPERSION_DELAY_FOR_FAST_BUY));
        informationAboutCurrentTrade.setDispersionDelayForBuy(getValueFromPreferences(DISPERSION_FOR_BUY, DEFAULT_DISPERSION_DELAY_FOR_BUY));
        informationAboutCurrentTrade.setDispersionDelayForFastBuy(getValueFromPreferences(DISPERSION_FOR_FAST_BUY, DEFAULT_DISPERSION_DELAY_FOR_FAST_BUY));
        informationAboutCurrentTrade.setPriceForFastBuy(priceForFastBuy);
    }

    private int getValueFromPreferences(String key, int defaultValue){
        return sPref.getInt(key, defaultValue);
    }

    private void updateInformationAboutTraderFromET(){
        updateCountForBuy();
        updateTraderFinishDateInMs();
        updateTopPriceLine();
        updatePriceForFastBuy();
    }

    protected Boolean checkConditionsForStartTrade(){
        return ((checkCountForBuy() | checkTraderTimeLive())  & checkTopPriceLine());
    }

    protected Boolean checkTraderTimeLive(){
        return traderTimeLiveInMs > 0;
    }

    protected Boolean checkTopPriceLine(){
        return elementTopPrice > 0;

    }

    protected Boolean checkCountForBuy() {
        return elementCountForBuy > 0;
    }

    private void successfulTradeStartMethods() {
        myAppBase.child(String.valueOf(currentTradeNumber)).setValue(informationAboutCurrentTrade);
        updateCurrentTradeNumber();
        createSuccessfulTraderStartToast().show();
        cleanTraderInformation();
    }

    private void updateCurrentTradeNumber(){
        currentTradeNumber++;
    }

    private void startTradeService() {
        startService(intent);
    }

    private void createIntentForTradeService() {
        intent = new Intent(this, TradeService.class);
    }

    protected Toast createToastForMissingValue() {
        return Toast.makeText(getApplicationContext(),
                R.string.toast_missing_some_data,
                Toast.LENGTH_LONG);
    }

    private Toast createSuccessfulTraderStartToast(){
        return Toast.makeText(getApplicationContext(),
                "Успешно запущен трейдер по " + currentElementRusName,
                Toast.LENGTH_LONG);
    }

    private void cleanTraderInformation(){
        etTraderTimeLive.setText("0");
        etPriceForFastBuy.setText("0");
        etElementsCountForBuy.setText("0");
        etTopPriceLine.setText("0");
    }

    //**************************

    private ArrayAdapter<?> makeAdapter() {
        ArrayAdapter<?> adapter =
                ArrayAdapter.createFromResource(this, R.array.elementList, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

    private void getLotInformation(int elementPosition) {
        chooseCurrentElementName(elementPosition);
        Network.getInstance().lotInformation(currentElementName, MainActivity.this);
    }

    private void chooseCurrentElementName(int elementPosition) {
        currentElementRusName = massiveOfElements[elementPosition];
        currentElementName = elementsNameMap.get(currentElementRusName);
    }

    private void createStringMassiveOfElements() {
        massiveOfElements = getResources().getStringArray(R.array.elementList);
    }


    private static HashMap<String, String> createHashMapForElements() {
        return new HashMap<String, String>(){
            {

                put("абразив", ABRASIVE);
                put("змеиный яд", SNAKE_POISON);
                put("клык тигра", TIGER_TUSK);
                put("ледяной кристалл", ICE_CRYSTAL);
                put("лунный камень", MOON_STONE);
                put("огненный кристалл", FIRE_CRYSTAL);
                put("осколок метеорита", METEORIT);
                put("цветок ведьм", WITCH_FLOWER);
                put("цветок ветров", WIND_FLOWER);
                put("цветок папоротника", FERN_FLOWER);
                put("ядовитый гриб", BADGRIB);
            }};
    }


    public void updateTVLowPrice(String price){
        tvCurrentPriceHint.setText(getResources().getString(R.string.tv_hint_current_price) + price);
    }

//    private void addSessionContainerToSharedPref(SessionDataContainer sessionDataContainer) {
//        sPref = getPreferences(MODE_PRIVATE);
//        SharedPreferences.Editor ed = sPref.edit();
//        ed.putString(PHPSESSID, sessionDataContainer.getPHPSESSID());
//        ed.putString(PL_ID, sessionDataContainer.getPl_id());
//        ed.putString(SID, sessionDataContainer.getSid());
//        ed.commit();
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        getChangingConfigurations();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_preferences) {
            startActivity(new Intent(this,PreferencesActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Ловим информацию о дешевом лоте

    @Override
    public void catchLotInformation(HashMap<String,String> information) {

        updateInterfaceInformation(information);
    }

    //*********************

    private void updateInterfaceInformation(HashMap<String, String> information) {
        updatePriceInformation(information);
        setDefaultValues();
    }

    private void setDefaultValues() {
        setETDefaultCountForBuy();
        setETDefaultTraderTimeLive();
    }

    private void setETDefaultCountForBuy(){
        etElementsCountForBuy.setText(String.valueOf(DEFAULT_COUNT_FOR_BUY));
        updateCountForBuy();
    }

    private void updateCountForBuy() {
        elementCountForBuy = Integer.parseInt(etElementsCountForBuy.getText().toString());
    }


    private void setETDefaultTraderTimeLive(){

        etTraderTimeLive.setText(DEFAULT_TRADER_TIME_LIVE);
        updateTraderTimeLive();
    }

    private void updateTraderTimeLive() {
        traderTimeLiveInMs = Long.parseLong(etTraderTimeLive.getText().toString())*MS_IN_MINUTE;
    }

    private void updateTraderFinishDateInMs() {
        updateTraderTimeLive();
        traderFinishDateInMs = Calendar.getInstance().getTimeInMillis() + traderTimeLiveInMs;
    }


    // ловим Нужные Куки после успешного логина

    @Override
    public void catchLoginCookies(String loginCookies) {
    }

    //**********************

    private void updatePriceInformation(HashMap<String, String> information) {
        updateTVLowPrice(information.get(SHOW_PRICE));
        updateEtTopPriceLine(information.get(SHOW_PRICE));
    }

    private void updateEtTopPriceLine(String defaultTopPriceLine) {
        etTopPriceLine.setText(defaultTopPriceLine);
        updateTopPriceLine();
    }

    private void updateTopPriceLine(){
        elementTopPrice = Integer.parseInt(etTopPriceLine.getText().toString());
    }

    private void updatePriceForFastBuy(){
        priceForFastBuy = Integer.parseInt(etPriceForFastBuy.getText().toString());
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}

