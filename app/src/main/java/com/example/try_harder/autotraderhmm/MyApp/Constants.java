package com.example.try_harder.autotraderhmm.myApp;

/**
 * Created by Try_harder on 21.07.2015.
 */
public interface Constants {
    String URL = "http://www.heroeswm.ru";
    String PLIV_2821 = "2821";
    String NICK = "trywinhere";
    String PASSWORD = "4mjqi3h3cawE";
    String loginRedirect = "1";
    String PHPSESSID = "PHPSESSID";
    String SID = "SID";
    String PL_ID = "PL_ID";
    String ABRASIVE = "abrasive";
    String SNAKE_POISON = "snake_poison";
    String TIGER_TUSK = "tiger_tusk";
    String ICE_CRYSTAL = "ice_crystal";
    String MOON_STONE = "moon_stone";
    String FIRE_CRYSTAL = "fire_crystal";
    String METEORIT = "meteorit";
    String WITCH_FLOWER = "witch_flower";
    String WIND_FLOWER = "wind_flower";
    String FERN_FLOWER = "fern_flower";
    String BADGRIB = "badgrib";
    String LOT_ID = "Lot_id";
    String TRADER_GOT_COUNT = "trader_got_count";
    String TRADE_OVER = "Торги закончены";
    String SHOW_ACTIVE_TRADERS = "show active traders";
    String SHOW_INACTIVE_TRADERS = "show inactive traders";
    String TV_BOUGHT = "Bought - ";
    String SHOW_PRICE = "Price: ";
    String BUY_ACCURACY = "Accuracy buy - ";
    String TRADE_WORK = "tradeWork";
    String FAST_BUY_ACCURACY = "Accuracy fast buy - ";
    String MY_BASE_URL = "https://autraderhmm.firebaseio.com";
    String DELAY_FOR_TRADE = "delayForTrade";
    String DELAY_FOR_BUY = "delayForBuy";
    String DELAY_FOR_FAST_BUY = "delayForFastBuy";
    String DISPERSION_FOR_BUY = "dispersionDelayForBuy";
    String DISPERSION_FOR_FAST_BUY = "dispersionDelayForFastBuy";
    String TEXT_DELAY_FOR_BUY = "Задержка перед покупкой - ";
    String TEXT_DELAY_FOR_FAST_BUY = "Задержка перед быстрой покупкой - ";
    String TEXT_DISPERSION_FOR_BUY = "Разброс задержки перед покупкой - ";
    String TEXT_DISPERSION_FOR_FAST_BUY = "Разброс задержки перед быстрой покупкой - ";
    String TEXT_DELAY_FOR_TRADE = "Основа задержки - ";
    String PREF_PATH = "Shared preferences";
    boolean ACTIVE_LIST = true;
    boolean INACTIVE_LIST = false;
    int DEFAULT_DELAY_FOR_TRADE = 2000;
    int DEFAULT_DELAY_FOR_BUY = 3000;
    int DEFAULT_DELAY_FOR_FAST_BUY = 1000;
    int DEFAULT_DISPERSION_DELAY_FOR_FAST_BUY = 500;
    int DEFAULT_DISPERSION_DELAY_FOR_BUY = 500;
    int CORE_POOL_SIZE = 20;
    int LONG_DELAY = 2;
    int NORMAL_DELAY = 1;
    int FIRST_LOT_NUMBER = 1;
    int INT_PHPSESSID = 11;
    int INT_PL_ID = 12;
    int INT_SID = 14;
    int DEFAULT_COUNT_FOR_BUY = 5;
    int MS_IN_MINUTE = 60000;
    String DEFAULT_TRADER_TIME_LIVE = "60";
    int BAD_TRADE = 0;

}
