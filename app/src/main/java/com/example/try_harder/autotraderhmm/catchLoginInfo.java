package com.example.try_harder.autotraderhmm;

import java.util.HashMap;

/**
 * Created by Try_harder on 25.07.2015.
 */
public interface CatchLoginInfo {
    void catchLoginCookies(String authorizationCookies);
}
