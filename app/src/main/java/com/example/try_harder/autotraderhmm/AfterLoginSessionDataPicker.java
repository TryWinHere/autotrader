package com.example.try_harder.autotraderhmm;

import java.util.HashMap;
import java.util.List;

import static com.example.try_harder.autotraderhmm.myApp.Constants.*;

import retrofit.client.Header;


/**
 * Created by Try_harder on 21.07.2015.
 */
public class AfterLoginSessionDataPicker {

    public HashMap<String,String> takeMainHeadersMap(List<Header> cookieHeader){
        return takeDataMap(cookieHeader);
    }

    private HashMap<String,String> takeDataMap(List<Header> cookieHeader) {
        HashMap<String,String> tempHashMap = new HashMap<>();
        fillHashMap(cookieHeader, tempHashMap);
        return tempHashMap;
        }

    private void fillHashMap(List<Header> cookieHeader, HashMap<String, String> tempHashMap) {
        tempHashMap.put(PHPSESSID, cleanPHPSESSID(getUncleanedCookiePHPSESSIDFromHeader(cookieHeader)));
        tempHashMap.put(PL_ID, cleanCookie(getUncleanedCookiePLIDFromHeader(cookieHeader)));
        tempHashMap.put(SID, cleanCookie(getUncleanedCookieSIDFromHeader(cookieHeader)));
    }

    String getUncleanedCookiePHPSESSIDFromHeader(List<Header> cookieHeader) {
        return cookieHeader.get(INT_PHPSESSID).getValue().toString();
    }

    String getUncleanedCookiePLIDFromHeader(List<Header> cookieHeader) {
        return cookieHeader.get(INT_PL_ID).getValue().toString();
    }

    String getUncleanedCookieSIDFromHeader(List<Header> cookieHeader) {
        return cookieHeader.get(INT_SID).getValue().toString();
    }

    String cleanPHPSESSID(String cookiePHPSESSID) {
        return cookiePHPSESSID.substring(cookiePHPSESSID.indexOf("=") + 1, cookiePHPSESSID.indexOf(";"));
    }

    String cleanCookie(String cookieHeader){
        return cookieHeader.substring(cookieHeader.indexOf("=") + 1);
    }
}

