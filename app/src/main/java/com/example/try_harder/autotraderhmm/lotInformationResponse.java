package com.example.try_harder.autotraderhmm;

import java.util.HashMap;

/**
 * Created by Try_harder on 23.07.2015.
 */
public interface LotInformationResponse {
     void catchLotInformation(HashMap<String,String> information);
}
