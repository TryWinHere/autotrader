package com.example.try_harder.autotraderhmm;

import android.support.annotation.NonNull;

import com.example.try_harder.autotraderhmm.Models.PageParser;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import retrofit.client.Response;

import static com.example.try_harder.autotraderhmm.myApp.Constants.*;

/**
 * Created by Try_harder on 28.07.2015.
 */
public class ParsePageForBoughtCount extends PageParser {

    private String parsedStringFromDocument;

    public int parsePage(Response response){

        getParsedStringFromDocument(response);
        if (parsedStringFromDocument.contains(TRADE_OVER)) return BAD_TRADE;
        return Integer.parseInt(getBoughtCountFromParsedStringResponse());
    }

    private void getParsedStringFromDocument(Response response) {
        parsedStringFromDocument =  getMainElement(getDocumentFromStringResponse(responseToStringConvert(response))).text();
    }

    @NonNull
    private String getBoughtCountFromParsedStringResponse() {
        return parsedStringFromDocument.substring(parsedStringFromDocument.lastIndexOf("\"") + 2, parsedStringFromDocument.lastIndexOf("ш") - 1);
    }


    @Override
    public Elements getMainElement(Document document) {
        return document.select(".wbwhite");
    }
}
