package com.example.try_harder.autotraderhmm;

import android.support.annotation.NonNull;

import com.squareup.okhttp.OkHttpClient;

import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.OkClient;
import retrofit.client.Response;


import static com.example.try_harder.autotraderhmm.myApp.Constants.NICK;
import static com.example.try_harder.autotraderhmm.myApp.Constants.PASSWORD;
import static com.example.try_harder.autotraderhmm.myApp.Constants.PHPSESSID;
import static com.example.try_harder.autotraderhmm.myApp.Constants.PLIV_2821;
import static com.example.try_harder.autotraderhmm.myApp.Constants.PL_ID;
import static com.example.try_harder.autotraderhmm.myApp.Constants.SID;
import static com.example.try_harder.autotraderhmm.myApp.Constants.URL;
import static com.example.try_harder.autotraderhmm.myApp.Constants.loginRedirect;

/**
 * Created by Try_harder on 22.07.2015.
 */
public class Network{

    private static Network mInstance = null;
    private RestAdapter restAdapter;
    private HashMap<String,String> sessionDataInformation;
    private RequestInterface mainRequestInterface;
    private ParsePageForLotInformation pageForLotInformationParser = new ParsePageForLotInformation();
    private ParsePageForBoughtCount parsePageForBoughtCount = new ParsePageForBoughtCount();

    public String authorizationCookie;

    private Network() {
        makeConfiguredRestAdapter();
        createMainRequestInterface();
    }

    public static Network getInstance(){
        if(mInstance == null){
            mInstance = new Network();
        }
        return mInstance;
    }

    public void loginHmmRetrofit() {
        mainRequestInterface.login(loginRedirect, NICK, PASSWORD, PLIV_2821, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
            }

            @Override
            public void failure(RetrofitError error) {
                sessionDataInformation = new AfterLoginSessionDataPicker().takeMainHeadersMap(takeCookieFromErrorResponse(error));
                authorizationCookieCreate();
//                instance.catchLoginCookies(authorizationCookie);  // отправляем куки
            }
        });
    }

    public void lotInformation(String elementName, final LotInformationResponse instance) {
        mainRequestInterface.elementSellPage(authorizationCookie, elementName, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                instance.catchLotInformation(pageForLotInformationParser.parsePage(response));
            }

            @Override
            public void failure(RetrofitError error) {

                error.fillInStackTrace();
            }
        });
    }

    public void buyElement(String lotid, final String count, final TradeInterface instance) {
        mainRequestInterface.buyElement(authorizationCookie, count, lotid, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                instance.tradeCatch(parsePageForBoughtCount.parsePage(response));
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private List<Header> takeCookieFromErrorResponse(RetrofitError error) {
        return error.getResponse().getHeaders();
    }

    private void authorizationCookieCreate(){
        authorizationCookie = ("PHPSESSID=" + sessionDataInformation.get(PHPSESSID) + "; pl_id=" + sessionDataInformation.get(PL_ID) + "; sid=" + sessionDataInformation.get(SID));
    }


    private void makeConfiguredRestAdapter() {

        restAdapter=  new RestAdapter.Builder()
                .setEndpoint(URL)
                .setRequestInterceptor(getConfiguredRequestInterceptor())
                .setClient(new OkClient(takeUnredirectedClient()))
                .build();
    }

    @NonNull
    private RequestInterceptor getConfiguredRequestInterceptor() {
        return new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Host", "www.heroeswm.ru");
            }
        };
    }


    private static OkHttpClient takeUnredirectedClient() {
        OkHttpClient client = new OkHttpClient();
        client.setFollowRedirects(false);
        return client;
    }

    private void createMainRequestInterface(){
        mainRequestInterface =  restAdapter.create(RequestInterface.class);
    }
}
