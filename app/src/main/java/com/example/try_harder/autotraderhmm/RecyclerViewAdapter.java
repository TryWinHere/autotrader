package com.example.try_harder.autotraderhmm;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.try_harder.autotraderhmm.Models.InformationForTrade;

import static com.example.try_harder.autotraderhmm.myApp.Constants.*;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Try_harder on 31.07.2015.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    //
    private ArrayList<InformationForTrade> informationForTradeArrayList;

    UserActionWithTrader instant;
    boolean activeList;

    //
    public RecyclerViewAdapter(ArrayList<InformationForTrade> informationForTradeList, UserActionWithTrader instant, boolean activeList) {
        this.informationForTradeArrayList = informationForTradeList;
        this.instant = instant;
        this.activeList = activeList;
    }

    //
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclertraderview_item, viewGroup, false);
        return new ViewHolder(view);
    }

    //
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {
        final InformationForTrade informationForTrade = informationForTradeArrayList.get(i);
        int iconResourceId = 0;

        updateTvTimer(viewHolder, informationForTrade);
        if (activeList) {
            viewHolder.itemView.setBackgroundResource(R.drawable.drawable_for_active_recycler_item);
        } else {
            viewHolder.itemView.setBackgroundResource(R.drawable.drawable_for_inactive_recycler_item);
        }
        viewHolder.traderName.setText(informationForTrade.getCurrentElementNameRus());
        viewHolder.tvPriceForBuy.setText(SHOW_PRICE + informationForTrade.getTopPrice());
        viewHolder.tvPriceForFastBuy.setText(SHOW_PRICE + informationForTrade.getPriceForFastBuy());
        viewHolder.tvBought.setText(TV_BOUGHT + informationForTrade.getBoughtCount() + "/" + informationForTrade.getNeededCountForBuy());
        viewHolder.tvBuyAccuracy.setText(BUY_ACCURACY + informationForTrade.getCountOfSuccessBuy() + "/" + informationForTrade.getTotalCountOfBuy());
        viewHolder.tvFastBuyAccuracy.setText(FAST_BUY_ACCURACY + informationForTrade.getCountOfSuccessFastBuy() + "/" + informationForTrade.getTotalCountOfFastBuy());
        viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete(i);
            }
        });
    }

    private void updateTvTimer(ViewHolder viewHolder, InformationForTrade informationForTrade) {
        Long timeLeftInMs = informationForTrade.getTimeLeft();
        Long minutes = TimeUnit.MILLISECONDS.toMinutes(timeLeftInMs);
        Long second = TimeUnit.MILLISECONDS.toSeconds(timeLeftInMs - TimeUnit.MINUTES.toMillis(minutes));
        viewHolder.tvTimer.setText(String.format("%d:%02d", minutes, second));
    }

    //
    @Override
    public int getItemCount() {
        return informationForTradeArrayList.size();
    }

    private void delete(int numberInArrayList) {

        instant.updateTrader(informationForTradeArrayList.get(numberInArrayList).getCurrentTradeNumber());
        informationForTradeArrayList.remove(numberInArrayList);
        notifyItemRemoved(numberInArrayList);

    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView traderName;
        private TextView tvBought;
        private TextView tvBuyAccuracy;
        private TextView tvFastBuyAccuracy;
        private TextView tvTimer;
        private TextView tvPriceForBuy;
        private TextView tvPriceForFastBuy;
        private Button deleteButton;

        public ViewHolder(View itemView) {
            super(itemView);
            tvPriceForBuy = (TextView) itemView.findViewById(R.id.tvPriceForBuy);
            tvPriceForFastBuy = (TextView) itemView.findViewById(R.id.tvPriceForFastBuy);
            tvTimer = (TextView) itemView.findViewById(R.id.tvTimer);
            traderName = (TextView) itemView.findViewById(R.id.tvTraderName);
            deleteButton = (Button) itemView.findViewById(R.id.recyclerViewItemDeleteButton);
            tvBought = (TextView) itemView.findViewById(R.id.tvBought);
            tvBuyAccuracy = (TextView) itemView.findViewById(R.id.tvBuyAccuracy);
            tvFastBuyAccuracy = (TextView) itemView.findViewById(R.id.tvFastBuyAccuracy);
        }
    }
}