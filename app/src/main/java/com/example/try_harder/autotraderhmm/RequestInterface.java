package com.example.try_harder.autotraderhmm;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Try_harder on 21.07.2015.
 */
public interface RequestInterface {
    @Headers({
            "Content-Type: application/x-www-form-urlencoded",
            "Cookie: PHPSESSID=b9230dfeca23733f0f2d3a5be2d8d85b"
})
    @FormUrlEncoded
    @POST("/login.php")
    void login(@Field("LOGIN_redirect") String loginRedirect, @Field("login") String nick, @Field("pass") String password, @Field("pliv") String pliv2821, Callback<Response> cb);

    @Headers({
            "Host: www.heroeswm.ru"
    })
    @GET("/auction.php?cat=elements&sort=4&type=0&sbn=1&sau=0")
    void elementSellPage(@Header("Cookie") String authorizationCookie, @Query("art_type") String art_type, Callback<Response> cb);

    @Headers({
            "Content-Type: application/x-www-form-urlencoded"
    })
    @FormUrlEncoded
    @POST("/auction_buy_now.php")
    void buyElement(@Header("Cookie") String authorizationCookie, @Field("count") String count, @Field("lotid") String lotid,  Callback<Response> cb);
}
